import random

JARAT = ' '
FAL = 'X'


# halmazok unióját képzni (mit és mivel a két halmaz)
def halmazok_unioja(hovatartozik, mit, mivel):
    for i in range(len(hovatartozik)):
        if hovatartozik[i] == mivel:
            hovatartozik[i] = mit


def maze(szel, mag):
    # ez tárolja, hogy melyik terem hova tartozik
    # először minden terem külön van
    hovatartozik = list(range(szel)) 

    # felső sor
    for x in range(szel * 2 + 1):
        print(FAL, end='')
    print()

    # gyártjuk a labirintust soronként
    for y in range(mag - 1):
        # EZ ITT EGY OLYAN SOR? AMELYIK TERMEKET TARTALMAZ
        # vagyis itt a vízszintes összeköttetések vannak
        print(FAL, end='')  # bal szélső fal
        for x in range(szel - 1):
            # ez a terem
            print(JARAT, end='')
            
            # és most jön az összeköttetés
            # csak akkor szabad összekötni őket, ha nincsenek egy halmazban
            if hovatartozik[x] != hovatartozik[x + 1]:
                if random.choice([True, False]):
                    # összekötődtek - közös halmaz
                    halmazok_unioja(hovatartozik, hovatartozik[x], hovatartozik[x + 1])
                    print(JARAT, end='')
                else:
                    # ha nem kötöttük össze, akkor itt fal van
                    print(FAL, end='')
            else:
                # mert egyébként nem szabad összekötni, szóval ide fixen fal
                print(FAL, end='')
        # az utolsó terem, fal, és a teremeket tartalmazó sor vége
        print(JARAT, end='')
        print(FAL) # jobb szélső fal

        # EZ MEG EGY TERMEKET NEM TARTALMAZÓ SOR
        print(FAL, end='')  # bal oldali fal
        # függőleges összeköttetések
        for x in range(szel):
            # ha egy elemű, muszáj bekötni
            if hovatartozik.count(hovatartozik[x]) == 1:
                print(JARAT, end='')
            else:
                # amúgy eldönthetjük, hogy bekötjük-e
                if random.choice([True, False]):
                    print(JARAT, end='')
                else:
                    print(FAL, end='')
                    # ha nem kötöttük be, akkor itt új halmaz keletkezik
                    hovatartozik[x] = max(hovatartozik) + 1
            
            # ez a termek közt átlósan elhelyezkedő fal, ez fix
            print(FAL, end='')
        # jobb szélső fal nem kell, mert az előbb a ciklus végén megvolt
        print()
    
    # az utolsó sor: minden különállót egyesíteni kell
    y = mag - 1
    print(FAL, end='')  # bal oldali fal
    for x in range(szel - 1):
        print(JARAT, end='')    # a terem
        if hovatartozik[x] != hovatartozik[x + 1]:
            print(JARAT, end='')
            halmazok_unioja(hovatartozik, hovatartozik[x], hovatartozik[x + 1])
        else:
            print(FAL, end='')
    print(JARAT, end='')    # utolsó járat
    print(FAL)  # jobb oldali fal

    # alsó sor
    for x in range(szel * 2 + 1):
        print(FAL, end='')
    print()


def main():
    szel = int(input('Hány terem legyen széltében? '))
    mag = int(input('Hány terem legyen magasságban? '))

    maze(szel, mag)


main()