import pygame
import pygame.gfxdraw
import pygame.time
import random


MERETX = 35
MERETY = 35
PIXEL = 16      # egy cella mérete
SLEEP = 40      # késleltetés

FAL = pygame.Color(255, 255, 255)
JARAT = pygame.Color(0, 0, 0)
IDEIGLENES = pygame.Color(0, 255, 0)

class Pont:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y


# rajzolás
def rajzol(ablak, kep, x, y, mit):    
    kep[y][x] = mit
    pygame.gfxdraw.box(ablak, pygame.Rect(x * PIXEL, y * PIXEL, PIXEL, PIXEL), mit)


# üres képet csinál
def ures_kep(ablak):
    # Üres 2D tömb
    kep = []
    for _ in range(MERETY):
        kep.append([None]*MERETX)

    # keret, mintha ott járat lenne, és akkor nem megy oda a rajzoló
    for y in range(MERETY):
        rajzol(ablak, kep, 0, y, JARAT)
        rajzol(ablak, kep, MERETX - 1, y, JARAT)

    for x in range(MERETX):
        rajzol(ablak, kep, x, 0, JARAT)
        rajzol(ablak, kep, x, MERETY - 1, JARAT)

    # többi telibe fallal
    for y in range(1, MERETY - 1):
        for x in range(1, MERETX - 1):
            rajzol(ablak, kep, x, y, FAL)

    return kep


# A labirintus generáló
def labirintus_rajzol(ablak, kep, kiindul_x, kiindul_y):
    pontok = []
    # iránykoordináták (delta x, delta y)
    # [balra, jobbra, fel, le]
    iranyok = [Pont(-1, 0), Pont(1, 0), Pont(0, 1), Pont(0, -1)]

    # a kiindulási pontot kivájjuk
    kep[kiindul_y][kiindul_x] = JARAT
    pontok.append(Pont(kiindul_x, kiindul_y))

    while 0 < len(pontok):
        # kiveszünk egy elemet a listából, ezt fogjuk vizsgálni.
        # itt lehet állítani, hogy első (0), utolsó (len(pontok) - 1)
        # vagy másmelyik element vegye ki a listából.
        pont = random.choice(pontok)

        # választunk egy random irányt is
        irany = random.choice(iranyok)
        cel = Pont(pont.x + irany.x * 2, pont.y + irany.y * 2)

        # megpróbálunk menni arra
        if kep[cel.y][cel.x] == FAL:
            rajzol(ablak, kep, cel.x, cel.y, IDEIGLENES)                    # új terem
            rajzol(ablak, kep, pont.x + irany.x, pont.y + irany.y, JARAT)   # odavezető út
            pontok.append(cel)
            pygame.display.update()
            pygame.event.pump()
            pygame.time.wait(SLEEP)

        # itt szebb lenne, ha összegyűjtenénk a lehetséges irányokat,
        # és utána abból választanánk véletlenszerűen, mert lehet, hogy
        # lehet menni valamerre, de a random pont nem azt az irányt
        # találja ki... összesen csak 4 irány van, úgyhogy ez a pongyolaság
        # most belefér

        # és akkor most, lehet még innen valahova menni?
        lehet_meg_valahova = False
        for i in range(4):
            cel = Pont(pont.x + iranyok[i].x * 2, pont.y + iranyok[i].y * 2)
            if kep[cel.y][cel.x] == FAL:
                lehet_meg_valahova = True

        # ha már nem, akkor kiszedjük a tárolóból
        if not lehet_meg_valahova:
            rajzol(ablak, kep, pont.x, pont.y, JARAT)
            pontok.remove(pont)
            pygame.display.update()
            pygame.event.pump()
            pygame.time.wait(SLEEP)


def main():
    pygame.init()
    ablak = pygame.display.set_mode((MERETX * PIXEL, MERETY * PIXEL))
    pygame.display.set_caption('Labirintus')

    kep = ures_kep(ablak)

    labirintus_rajzol(ablak, kep, int(MERETX / 4) * 2, int(MERETY / 4) * 2)

    pygame.time.wait(5000)

    # ablak bezárása
    pygame.quit()


main()
