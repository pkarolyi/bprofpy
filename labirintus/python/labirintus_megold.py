MERETX = 19
MERETY = 13

JARAT = ' '
FAL = 'X'
KIJARAT = '*'
ZSAKUTCA = '.'
KERDESES = '?'


# kirajzolás
def kirajzol(palya):
    for y in range(MERETY):
        for x in range(MERETX):
            print(palya[y][x], end='')
        print()


# igazzal tér vissza, ha arrafelé megtalálta, hamissal, ha nem
def megfejt(palya, x, y, cel_x, cel_y):
    # első körben bejelöljük ezt a helyet KERDESES-nek -
    # ez végülis csak azért lényeges, hogy ne járat legyen itt,
    # mert akkor visszajönne ide
    palya[y][x] = KERDESES

    # egyelőre nem találjuk a kijáratot
    megtalalt = False

    # pont a célnál állunk?
    if x == cel_x and y == cel_y:
        megtalalt = True

    # ha még nem találtuk meg a kijáratot... és tudunk jobbra menni
    if not megtalalt and x < MERETX - 1 and palya[y][x + 1] == JARAT:
        # ha arra van a megfejtés
        if megfejt(palya, x + 1, y, cel_x, cel_y):
            megtalalt = True

    # ...balra
    if not megtalalt and x > 0 and palya[y][x - 1] == JARAT:
        if megfejt(palya, x - 1, y, cel_x, cel_y):
            megtalalt = True

    if not megtalalt and y > 0 and palya[y - 1][x] == JARAT:
        if megfejt(palya, x, y - 1, cel_x, cel_y):
            megtalalt = True

    if not megtalalt and y < MERETY - 1 and palya[y + 1][x] == JARAT:
        if megfejt(palya, x, y + 1, cel_x, cel_y):
            megtalalt = True
    
    # viszont ha innen kiindulva meg lehet találni a kijáratot
    # (most már tudjuk a függvények visszatérési értékéből),
    # akkor a kijárathoz vezető útként jelöljük meg.
    palya[y][x] = KIJARAT if megtalalt else ZSAKUTCA

    # jelezzük a hívónak, hogy valahonnan errefelé indulva
    # lehet-e kijáratot találni
    return megtalalt


def main():
    # sztringekkel adjuk meg a pályát, hogy jól látsszon 
    # a forráskódban a bemenet. Mivel pythonban a sztringek
    # immutable-ek, listákká kell alakítani. A lista elemei
    # a szting karakterei lesznek (pl. list('XYZ') -> ['X', 'Y', 'Z'])
    palya = [
        list('XXXXXXXXXXXXXXXXXXX'),
        list('      X X     X   X'),
        list('XXXXX X X XXX X X X'),
        list('X X   X   X X   X X'),
        list('X X XXX XXX XXXXX X'),
        list('X X X       X     X'),
        list('X X XXXXX XXX XXX X'),
        list('X X     X X   X   X'),
        list('X XXXXX XXX XXX XXX'),
        list('X     X     X X X X'),
        list('X XXXXXXXXXXX X X X'),
        list('X                  '),
        list('XXXXXXXXXXXXXXXXXXX')
    ]
    
    # honnan indulunk és hol a jutalomfalat
    megfejt(palya, 0, 1, MERETX - 1, MERETY - 2)
    kirajzol(palya)


main()
