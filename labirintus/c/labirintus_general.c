#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MERETX 35
#define MERETY 15

/* szandekosan karakterkodok, igy konnyu printfelni */
typedef enum Cella { Jarat = ' ', Fal = 'X' } Cella;
typedef Cella Palya[MERETY][MERETX];

/* az egesz palyat fallal tolti ki */
void ures(Palya p) {
    int x, y;

    for (y = 0; y < MERETY; ++y)
        for (x = 0; x < MERETX; ++x)
            p[y][x] = Fal;
}

/* kirajzolja */
void kirajzol(Palya p) {
    int x, y;

    for (y = 0; y < MERETY; ++y) {
        for (x = 0; x < MERETX; ++x)
            /* itt hasznalja ki, hogy az enum ertekek szandekosan
             * egyeznek a karakterkodokkal */
            putchar(p[y][x]);
        putchar('\n');
    }
}

/* ez maga a generalo fuggveny */
void labirintus(Palya p, int x, int y) {
    typedef enum { fel, le, jobbra, balra } Irany;
    Irany iranyok[4] = {fel, le, jobbra, balra};
    int i;

    /* erre a pontra hivtak minket, ide lerakunk egy darabka jaratot. */
    p[y][x] = Jarat;

    /* a tomb keverese */
    for (i = 0; i < 4; ++i) {       /* mindegyiket... */
        int r = rand() % 4;         /* egy veletlenszeruen valasztottal... */
        Irany temp = iranyok[i];    /* megcsereljuk. */
        iranyok[i] = iranyok[r];
        iranyok[r] = temp;
    }

    /* a kevert iranyok szerint mindenfele probalunk menni, ha lehet. */
    for (i = 0; i < 4; ++i)
        switch (iranyok[i]) {
        case fel:
            if (y >= 2 && p[y - 2][x] != Jarat) {
                p[y - 1][x] = Jarat;      /* elinditjuk arrafele a jaratot */
                labirintus(p, x, y - 2); /* es rekurzive megyunk tovabb */
            }
            break;
        case balra:
            if (x >= 2 && p[y][x - 2] != Jarat) {
                p[y][x - 1] = Jarat;
                labirintus(p, x - 2, y);
            }
            break;
        case le:
            if (y < MERETY - 2 && p[y + 2][x] != Jarat) {
                p[y + 1][x] = Jarat;
                labirintus(p, x, y + 2);
            }
            break;
        case jobbra:
            if (x < MERETX - 2 && p[y][x + 2] != Jarat) {
                p[y][x + 1] = Jarat;
                labirintus(p, x + 2, y);
            }
            break;
        }
}


int main(void) {
    Palya p;

    srand(time(0));
    /* ures palyara general egy labirintust */
    ures(p);
    labirintus(p, 1, 1);
    /* bejarat es kijarat */
    p[1][0] = Jarat;
    p[MERETY - 2][MERETX - 1] = Jarat;
    /* mehet kirajzolasra */
    kirajzol(p);

    return 0;
}
