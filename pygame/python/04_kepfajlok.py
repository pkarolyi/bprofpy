import pygame
import pygame.gfxdraw


class Pieces:
    def __init__(self):
        # a figurák koordinátái a képen
        self.W_KING = (10, 10)
        self.W_QUEEN = (72, 10)
        self.W_ROOK = (134, 10)
        self.W_BISHOP = (196, 10)
        self.W_KNIGHT = (258, 10)
        self.W_PAWN = (320, 10)
        self.B_KING = (10, 70)
        self.B_QUEEN = (72, 70)
        self.B_ROOK = (134, 70)
        self.B_BISHOP = (196, 70)
        self.B_KNIGHT = (258, 70)
        self.B_PAWN = (320, 70)

        # a figurák mérete a képen
        self.SIZE = (62, 62)

        # kép betöltése
        self.image = pygame.image.load('pieces.png')


def main():
    # struktúra létrehozása
    pieces = Pieces()

    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((320, 200))
    pygame.display.set_caption('pygame példaprogram')

    # rajz készítése
    window.fill(pygame.Color('#90E090'))
    window.blit(pieces.image, (82, 74), pygame.Rect(pieces.W_KING, pieces.SIZE))
    window.blit(pieces.image, (144, 74), pygame.Rect(pieces.B_PAWN, pieces.SIZE))
    window.blit(pieces.image, (206, 74), pygame.Rect(pieces.W_KNIGHT, pieces.SIZE))

    # ki a képernyőre
    pygame.display.update()

    # szokásos várakozás a kilépésre
    quit = False
    while not quit:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            quit = True

    pygame.quit()


main()
