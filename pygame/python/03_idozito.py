import pygame
import pygame.gfxdraw

# ablak mérete és labda sugara
WINDOW = 360
BALL_R = 10


# labda struktúra
class Ball:
    def __init__(self):
        self.x = WINDOW // 2
        self.y = WINDOW // 2
        self.vx = 3
        self.vy = 2


def main():
    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((360, 360))
    pygame.display.set_caption('pygame példaprogram')

    # struktúra létrehozása
    ball = Ball()

    # időzítő hozzáadása: 20ms; 1000 ms / 20 ms -> 50 fps
    pygame.time.set_timer(pygame.USEREVENT, 20)

    quit = False
    # szokásos eseményhurok
    while not quit:
        event = pygame.event.wait()
        # felhasználói esemény: ilyeneket generál az időzítő függvény
        if event.type == pygame.USEREVENT:
            # kitöröljük az előző pozíciójából (nagyjából)
            pygame.gfxdraw.filled_circle(window, ball.x, ball.y, BALL_R, pygame.Color('#202040'))
            # kiszámítjuk az új helyet
            ball.x += ball.vx
            ball.y += ball.vy
            # visszapattanás
            if (ball.x < BALL_R or ball.x > WINDOW - BALL_R):
                ball.vx *= -1
            if (ball.y < BALL_R or ball.y > WINDOW - BALL_R):
                ball.vy *= -1
            # újra kirajzolás, és mehet a képernyőre
            pygame.gfxdraw.filled_circle(window, ball.x, ball.y, BALL_R, pygame.Color('#8080FF'))
            pygame.display.update()

        if event.type == pygame.QUIT:
            quit = True

    # időzítő törlése
    pygame.time.set_timer(pygame.USEREVENT, 0)
    pygame.quit()


main()
