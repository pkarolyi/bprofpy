import math
import pygame
import pygame.gfxdraw


def main():
    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((440, 360))
    pygame.display.set_caption('pygame példaprogram')

    # színek
    red = pygame.Color(255, 0, 0)
    green = pygame.Color(0, 255, 0)
    blue = pygame.Color(0, 0, 255)

    # pozíció, sugár
    x = 100
    y = 100
    r = 50

    # kör
    pygame.gfxdraw.circle(window, x, y, r, red)
    pygame.gfxdraw.circle(window, x + r, y, r, green)
    pygame.gfxdraw.circle(window, math.floor(x + r * math.cos(3.1415 / 3)), math.floor(y -  r * math.sin(3.1415 / 3)), r, blue)

    x = 280
    y = 100

    # antialias kör
    pygame.gfxdraw.aacircle(window, x, y, r, red)
    pygame.gfxdraw.aacircle(window, x + r, y, r, green)
    pygame.gfxdraw.aacircle(window, math.floor(x + r * math.cos(3.1415 / 3)), math.floor(y -  r * math.sin(3.1415 / 3)), r, blue)

    x = 100
    y = 280

    # kitöltött kör
    pygame.gfxdraw.filled_circle(window, x, y, r, red)
    pygame.gfxdraw.filled_circle(window, x + r, y, r, green)
    pygame.gfxdraw.filled_circle(window, math.floor(x + r * math.cos(3.1415 / 3)), math.floor(y -  r * math.sin(3.1415 / 3)), r, blue)

    x = 280
    y = 280

    # áttetsző színek
    translucent_red = pygame.Color(255, 0, 0, 96)
    translucent_green = pygame.Color(0, 255, 0, 96)
    translucent_blue = pygame.Color(0, 0, 255, 96)

    # áttetsző kör
    pygame.gfxdraw.filled_circle(window, x, y, r, translucent_red)
    pygame.gfxdraw.filled_circle(window, x + r, y, r, translucent_green)
    pygame.gfxdraw.filled_circle(window, math.floor(x + r * math.cos(3.1415 / 3)), math.floor(y -  r * math.sin(3.1415 / 3)), r, translucent_blue)

    # az elvégzett rajzolások a képernyőre
    pygame.display.update()

    # várunk kilépésre
    quit = False
    while not quit:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            quit = True

    # ablak bezárása
    pygame.quit()


main()
